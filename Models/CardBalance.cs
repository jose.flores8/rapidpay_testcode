﻿namespace RapidPay_API.Models
{
    public class CardBalance
    {
        public int Total { get; set; }
        public double TotalFee { get; set; }
    }
}
