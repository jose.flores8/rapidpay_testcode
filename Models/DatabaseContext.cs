﻿using Microsoft.EntityFrameworkCore;

namespace RapidPay_API.Models
{
    public partial  class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User>? UserInfos { get; set; }
        public virtual DbSet<Card>? Cards { get; set; }

        public virtual DbSet<Balance>? CardBalances { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasNoKey();
                entity.ToTable("UserInfo");
                entity.Property(e => e.UserId).HasColumnName("UserId");
                entity.Property(e => e.DisplayName).HasMaxLength(60).IsUnicode(false);
                entity.Property(e => e.UserName).HasMaxLength(30).IsUnicode(false);
                entity.Property(e => e.Email).HasMaxLength(50).IsUnicode(false);
                entity.Property(e => e.Password).HasMaxLength(20).IsUnicode(false);
                entity.Property(e => e.CreatedDate).IsUnicode(false);
            });


            modelBuilder.Entity<Card>(entity =>
            {
                entity.ToTable("Card");
                entity.Property(e => e.CardId).HasColumnName("CardId");
                entity.Property(e => e.Number).HasMaxLength(15).IsUnicode(false);
                entity.Property(e => e.HolderName).HasMaxLength(100).IsUnicode(false);
                entity.Property(e => e.ExpDate).HasMaxLength(5).IsUnicode(false);
                entity.Property(e => e.CvdCode).HasMaxLength(3).IsUnicode(false);
                entity.Property(e => e.ClientId).HasColumnName("UserId").IsUnicode(false);               
            });

            modelBuilder.Entity<Balance>(entity =>
            {
                entity.ToTable("CardBalance");
                entity.Property(e => e.CardId).HasColumnName("CardId");
                entity.Property(e => e.Total).IsUnicode(false);
                entity.Property(e => e.OperationDate).HasColumnName("PaymentDate").IsUnicode(false);
                entity.Property(e => e.FeeAmount).IsUnicode(false);
                entity.Property(e => e.FeeRate).IsUnicode(false);


            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

