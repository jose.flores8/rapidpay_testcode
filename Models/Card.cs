﻿using Microsoft.AspNetCore.Authentication;
using System.ComponentModel.DataAnnotations;

namespace RapidPay_API.Models
{
    public class Card
      {

        public int CardId { get; set; }

        [Required]
        [MaxLength(15)]
        public string Number { get; set; }

        [Required]
        public string HolderName  { get; set; }

        [Required]
        public string ExpDate { get; set; }

        [Required]
        public string CvdCode { get; set; }

        [Required]
        public int ClientId { get; set; }
    }
}
