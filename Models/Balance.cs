﻿
namespace RapidPay_API.Models
{
    public class Balance
    {
        public int BalanceId { get; set; }
        public int CardId { get; set; }
        public int Total { get; set; }
        public DateTime OperationDate { get; set; }
        public double FeeRate  { get; set; }
        public double FeeAmount {
            get
            {
                return (Total * FeeRate);

            }
            set { }
         }

    }

}
