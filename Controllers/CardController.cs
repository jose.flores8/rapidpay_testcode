﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RapidPay_API.Interface;
using RapidPay_API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RapidPay_API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    
    public class CardController : ControllerBase
    {


        private readonly ICard _ICard;

        public CardController(ICard Icard)
        {
            _ICard = Icard;
        }


        // POST api/<CardController>   
        [HttpPost]
        public async Task<ActionResult<Card>> Post(Card card)
        {
            _ICard.AddCard(card);
            return await Task.FromResult(card);
        }

    }
}
