﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RapidPay_API.Interface;
using RapidPay_API.Models;

namespace RapidPay_API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/Pay")]
    public class PayController : Controller
    {
        private readonly IPay _IPay;

        public PayController(IPay Ipay)
        {
            _IPay = Ipay;
        }

        [HttpPost]
        public async Task<ActionResult<Balance>> Post(Balance balance)
        {
            _IPay.AddPayOperation(balance);
            return await Task.FromResult(balance);
        }
    }
}
