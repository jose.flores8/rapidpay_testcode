﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RapidPay_API.Interface;
using RapidPay_API.Models;

namespace RapidPay_API.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]

    public class CardBalanceController : ControllerBase   
    {
        private readonly IBalance _IBalance;

        public CardBalanceController(IBalance Ibalance)
        {
            _IBalance = Ibalance;
        }


        // POST api/<CardController>   
        [HttpGet]
        public async Task<ActionResult<CardBalance>> Get(int cardId)
        {
          var balance  = _IBalance.GetBalance(cardId);
            return await Task.FromResult(balance);
        }
    }
}
