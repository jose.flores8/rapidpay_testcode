﻿
using RapidPay_API.Interface;
using RapidPay_API.Models;

namespace RapidPay_API.Repository
{
    public class BalanceRepository : IBalance
    {

        readonly DatabaseContext _dbContext = new();

        public BalanceRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }


        public CardBalance GetBalance(int cardId)
        {
                var cardBalanceCharge = _dbContext.CardBalances.Where(c => c.CardId == cardId).Sum(c=> c.Total);
                var cardBalanceFees = _dbContext.CardBalances.Where(c => c.CardId == cardId).Sum(c => c.FeeAmount);



            return new CardBalance{
                    Total = cardBalanceCharge,   
                    TotalFee= cardBalanceFees,                   
                };
        }
    }
}
