﻿using Microsoft.EntityFrameworkCore;
using RapidPay_API.Interface;
using RapidPay_API.Models;

namespace RapidPay_API.Repository
{
    public class CardRepository : ICard
    {

        readonly DatabaseContext _dbContext = new();

        public CardRepository (DatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddCard(Card card)
        {            
                try
                {
                    _dbContext.Cards.Add(card);
                    _dbContext.SaveChanges();
                }
                catch (Exception ex)
                {
                    
                }
            
        }
    }
}
