﻿using Microsoft.EntityFrameworkCore;
using RapidPay_API.Interface;
using RapidPay_API.Models;

namespace RapidPay_API.Repository
{
    public class PayRepository : IPay
    {

        readonly DatabaseContext _dbContext = new();
        readonly IPaymentFeeService _paymentFeeService;


        public PayRepository(DatabaseContext dbContext, IPaymentFeeService paymentFeeService)
        {
            _dbContext = dbContext;
            _paymentFeeService = paymentFeeService;
        }

        public void AddPayOperation(Balance balance)
        {

            try
            {

                balance.FeeRate = _paymentFeeService.CalculateFeeRate();
                _dbContext.CardBalances.Add(balance);
                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }
    }
}
