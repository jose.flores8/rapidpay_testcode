﻿using RapidPay_API.Models;

namespace RapidPay_API.Interface
{
    public interface ICard
    {
        public void AddCard(Card employee);
    }
}
