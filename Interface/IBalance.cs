﻿using RapidPay_API.Models;

namespace RapidPay_API.Interface
{
    public interface IBalance
    {
        public CardBalance GetBalance(int cardId);

    }

}
