﻿namespace RapidPay_API.Interface
{
    public interface IPaymentFeeService
    {
        public double CalculateFeeRate();
    }
}
