﻿using RapidPay_API.Models;

namespace RapidPay_API.Interface
{
    public interface IPay
    {

        public void AddPayOperation(Balance balance);
    }
}
