﻿using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RapidPay_API.Interface;

namespace RapidPay_API.Services
{
    public class PaymentFeeService : IPaymentFeeService
    {

        private Random random = new Random();
        private DateTime lastDateRandomGenerated = DateTime.Now;
        private double  rateFee = -1;

        public double   CalculateFeeRate()
        {
            if(rateFee == -1 || lastDateRandomGenerated.AddHours(1) < DateTime.Now)  
            {
                rateFee = random.Next(2);
                lastDateRandomGenerated = DateTime.Now;
            }
            return rateFee * 0.1;
            
        }
    }
}
